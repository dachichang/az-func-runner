FROM ubuntu:18.04

# install the tools
RUN apt update && apt install -y wget curl
# install azure-cli
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash
# install microsoft package source tree
RUN wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb && dpkg -i packages-microsoft-prod.deb
# update and install azure-functions-core-tools and reqirement packages
RUN apt update && apt install -y \
    libicu-dev \
    azure-functions-core-tools-3 \
    python3.7 \
    python3-pip \
 && rm -rf /var/lib/apt/lists/*
RUN ln -sf python3.7 /usr/bin/python3
RUN pip3 install virtualenv
